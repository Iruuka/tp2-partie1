﻿using System;

namespace tp2partie1Console
{
    public class Program
    {
        public static string TestPhrase(string phrase)
        {
            int taille=phrase.Length;
            bool bopoint, bomaj;
            if (phrase[(phrase.Length)-1]=='.')
            {
                bopoint=true;
            }
            else{
                bopoint=false;
            }
            if (phrase[0]>='A' && phrase[0]<='Z')
            {
                bomaj=true;
            }
            else
            {
                bomaj=false;
            }
            if (bomaj==true)
            {
                if (bopoint==true)
                {
                    return ("majuscule et point");
                }
                else
                {
                    return ("majuscule et paspoint");
                }
            }
            else
            {
                if (bopoint==true)
                {
                    return ("pasmajuscule et point");
                }
                else
                {
                    return ("pasmajuscule et paspoint");
                }
            }
        }
        
        static void Main(string[]args)
        {
            string phrase, final="";

            Console.WriteLine("Veuillez écrire une phrase");
            phrase=Console.ReadLine();

            final=TestPhrase(phrase);
            Console.WriteLine("");
            Console.WriteLine(final);
            Console.WriteLine("");
        }
    }
}
