using System;
using Xunit;
using tp2partie1Console;
namespace tp2partie1Test
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            Assert.Equal("majuscule et point", Program.TestPhrase("Bonjour je suis un connard."));
            Assert.Equal("pasmajuscule et point", Program.TestPhrase("bien alors...."));
            Assert.Equal("majuscule et paspoint", Program.TestPhrase("Hola... ?"));
            Assert.Equal("pasmajuscule et paspoint", Program.TestPhrase("$afefggv"));
        }
    }
}
